
	  ***************************
	  *   The WindowLabyrinth   *
	  ***************************

Written by Thomas Huth, 1995-1997

WinLaby is Public Domain software. That means everybody is allowed to change
the sources and redistribute them. Anyway, I'd like to ask you to keep at least
a small remark in the game that the original has been written by me. I'd also
be glad if you write me an e-mail about the game (especially if you did any
interesting modifications).

Here's my address: huth(at)tuxfamily.org

Some words about the program:
Your mission is to find the exit of a maze. You can walk around using the
cursor keys or using the keypad keys: '8' = forward, '4' = turn left, '6' = turn
right and '2' = backwards. The rest should be pretty self-explanatory.
