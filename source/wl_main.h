
#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

/* Externe Variablen: */
extern int vhandle, ap_id;
extern int work_in[], work_out[];
extern void *tree_adr;
extern int wi_handle;
extern int deskx, desky, deskw, deskh;
extern GRECT wnd;
extern int xy[];
extern int xpos, ypos;
extern int zielx, ziely;
extern unsigned char orttyp[21][21];
extern int richtung;
extern int pmode;


/* Prototypes: */
void fehler(char *text);
